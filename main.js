// TOGGLE MENU BUTTON
var headerMenu = document.querySelector(".headerMenu__wrapper");
var openMenuButton = document.querySelector(".headerMenu-button");
var closeMenu = document.querySelector(".headerMenu__wrapper__lists-closeMenu");

openMenuButton.addEventListener("click", () => {
  headerMenu.style.display = "block";
});

closeMenu.addEventListener("click", () => {
  headerMenu.style.display = "none";
});

// OPEN MENU LIST ITEM WHEN RESPONSIVE
var plusIcon = document.querySelector(".c-l-title-respon-button");
var icon = document.querySelector(".c-l-title-respon-button i");
var menuItem = document.querySelector(".c-l-menu-wrapper");

plusIcon.addEventListener("click", () => {
  menuItem.classList.toggle("active");

  if (menuItem.classList.value.includes("active")) {
    icon.classList.remove("fa-plus");
    icon.classList.add("fa-minus");
  } else {
    icon.classList.remove("fa-minus");
    icon.classList.add("fa-plus");
  }
});

// HANDLE EVENT SLIDE IN BANNER
var slideIndex = 0;
var timeOut;
var slides = document.getElementsByClassName("slide");
var dots = document.getElementsByClassName("dot");
showSlides();
function currentSlide(n) {
  var i;
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex = n;
  slides[n - 1].style.display = "block";

  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  dots[slideIndex - 1].className += " active";
  clearTimeout(timeOut);
}
function plusSlide(n) {
  var newslideIndex = slideIndex + n;
  if (newslideIndex < 5 && newslideIndex > 0) {
    currentSlide(newslideIndex);
  }
}
function showSlides() {
  var i;

  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {
    slideIndex = 1;
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " active";

  timeOut = setTimeout(showSlides, 3000); // Change image every 2 seconds
}


